FROM node:16 as build
WORKDIR /app
RUN git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
WORKDIR 2048-game
RUN npm install --include=dev
RUN npm run build

FROM node:16-alpine
COPY --from=build /app /app
WORKDIR /app/2048-game
ENTRYPOINT ["npm", "start"]
